packer {
  required_plugins {
    openstack = {
      version = "~> 1"
      source  = "github.com/hashicorp/openstack"
    }
  }
}

source "openstack" "ubuntu_focal_base" {
  flavor                    = "s1-2"
  image_name                = "ubuntu_focal_base"
  image_tags                = ["base", "ubuntu", "focal"]
  networks                  = ["79b4242b-5511-491d-918a-053a332f0686"]
  external_source_image_url = "https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64.img"
  security_groups           = ["default"]
  ssh_username              = "ubuntu"
  volume_type               = "rbd"
  volume_size               = "5"
}

build {
  sources = ["source.openstack.ubuntu_focal_base"]

  provisioner "shell" {
    inline = [
      "sudo apt-get update",
      "sudo apt-get -y upgrade"
    ]
  }
}
