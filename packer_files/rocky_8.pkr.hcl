packer {
  required_plugins {
    openstack = {
      version = "~> 1"
      source  = "github.com/hashicorp/openstack"
    }
  }
}


source "openstack" "rocky_8_base" {
  flavor                    = "s1-2"
  image_name                = "rocky_8_base"
  image_tags                = ["base", "rocky", "8"]
  networks                  = ["79b4242b-5511-491d-918a-053a332f0686"]
  external_source_image_url = "https://dl.rockylinux.org/pub/rocky/8/images/x86_64/Rocky-8-GenericCloud.latest.x86_64.qcow2"
  ssh_username              = "rocky"
  security_groups           = ["default"]
  image_visibility          = "public"
  use_blockstorage_volume   = true
  volume_type               = "nvme"
  volume_size               = "5"
}

build {
  sources = ["source.openstack.rocky_8_base"]

  provisioner "shell" {
    inline = [
      "sudo dnf -y upgrade"
    ]
  }
}
